/*
** list.h for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:49:38 2014 lionel karmes
** Last update Tue Jan  6 17:30:51 2015 lionel karmes
*/

#ifndef PARAMS_LIST_H_
# define PARAMS_LIST_H_

typedef struct		s_element
{
  char			*e_data;
  int			len;
  int			id;
  int			selected;
  struct s_element	*e_next;
  struct s_element	*e_prev;
}			t_element;

typedef struct		s_list
{
  int			size;
  t_element		*l_start;
  t_element		*l_end;
  t_element		*l_current;
}			t_list;

#endif /* !PARAMS_LIST_H_ */
