/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Fri Jan  9 18:29:18 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <sys/ioctl.h>
# include <ncurses/curses.h>
# include <term.h>
# include <termios.h>
# include <signal.h>
# include <stdlib.h>
# include <unistd.h>
# include "list.h"
# include "cursor.h"
# include "events.h"

# define MAX(v1, v2)	((v1) > (v2)) ? (v1) : (v2)

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putchar0(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstr0(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
int		my_select(int, char **);
t_list		*init_list(int, char **);
t_list		*new_list();
void		remove_list(t_list **);
void		remove_element(t_list **, t_element *);
t_element	*find_element(t_list *, int);
void		my_elementcpy(t_element *, t_element *);
void		my_put_in_list_end(t_list **, char *, int);
void		my_put_in_list_start(t_list **, char *, int);
int		print_list(t_position *, t_list *);
int		read_key(t_position *, t_list *);
int		my_putchar2(int);
int		cursor_move(char *,t_position *, t_list **);
int		my_cursor_left(t_position *);
int		my_cursor_right(t_position *);
int		my_cursor_up(t_position *);
int		my_cursor_down(t_position *);
struct termios	init_termios();
void		modify_terminal(struct termios *);
void		raw_mod(struct termios *);
void		cano_mod(struct termios *);
int		pos_cursor_x(int, t_cursor *);
void		pos_cursor_choice(int, int);
void		pos_cursor_end();
void		pos_cursor_begin_word(t_cursor *);
void		select_word(t_position *, t_list **);
void		clear_home();
void		underline(t_list *, t_list *);
void		underline_str(t_position *, t_list *);
void		reverse_mod(t_list *, t_element *);
int		update_list(t_position *, t_list **);
int		*my_int_tab_realloc(int *, int, int);
void		get_sigwinch(int);
void		signal_win(t_position *, t_list *);
void		print_list_select(t_list *);

#endif /* !MY_H_ */
