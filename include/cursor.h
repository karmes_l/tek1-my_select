/*
** cursor.h for  in /home/karmes_l/Projets/Systeme_Unix/my_select/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 29 10:02:53 2014 lionel karmes
** Last update Thu Jan  8 17:52:13 2015 lionel karmes
*/

#ifndef CURSOR_H_
# define CURSOR_H_

typedef struct		s_cursor
{
  int			x;
  int			y;
  int			*len_column;
  int			size;
}			t_cursor;

typedef struct		s_position
{
  t_list		*list;
  t_cursor		cursor;
}			t_position;

# define END		(0)

#endif /* !CURSOR_H */
