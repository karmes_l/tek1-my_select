/*
** events.h for  in /home/karmes_l/Projets/Igraph/Wolf3D/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 20 11:47:45 2014 lionel karmes
** Last update Mon Dec 29 13:51:13 2014 lionel karmes
*/

#ifndef EVENTS_H_
# define EVENTS_H_

# define K_ECHAP (27)
# define K_RIGHT "[C"
# define K_LEFT "[D"
# define K_UP "[A"
# define K_DOWN "[B"

#endif /* !EVENTS_H_ */

