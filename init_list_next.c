/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Mon Jan  5 10:41:10 2015 lionel karmes
*/

#include "my.h"

t_element	*find_element(t_list *list, int id)
{
  int		i;
  t_element	*tmp;

  i = 0;
  tmp = list->l_start;
  while (i < list->size)
    {
      if (tmp->id == id)
	return (tmp);
      else
	tmp = tmp->e_next;
      i++;
    }
  return (NULL);
}

void		remove_element_next(t_list **list, t_element *element)
{
 t_element	*next;
 t_element	*prev;
 
 next = element->e_next;
 prev = element->e_prev;
 
 if (next == NULL)
   {
     prev->e_next = NULL;
     (*list)->l_end = prev;
   }
 else if (prev == NULL)
   {
     next->e_prev = NULL;
     (*list)->l_start = next;
   }
 else
   {
     next->e_prev = prev;
     prev->e_next = next;
   }
}

void		remove_element(t_list **list, t_element *element)
{
  if ((*list)->size > 1)
    remove_element_next(list, element);
  else
    {
      (*list)->l_end = NULL;
      (*list)->l_start = NULL;
    }
  free(element);
  (*list)->size--;
}

void		my_elementcpy(t_element *dest, t_element *src)
{
  dest->id = src->id;
  dest->e_data = src->e_data;
  dest->len =  src->len;
  dest->e_next = src->e_next;
  dest->e_prev = src->e_prev;
}
