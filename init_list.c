/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Fri Jan  9 16:06:46 2015 lionel karmes
*/

#include "my.h"

void		my_put_in_list_end(t_list **list, char *str, int id)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->len = my_strlen(str);
  if ((element->e_data = malloc(sizeof(char) * (element->len + 1))) == NULL)
    exit(0);
  my_strcpy(element->e_data, str);
  element->id = id;
  element->e_next = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_prev = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_end->e_next = element;
      element->e_prev = (*list)->l_end;
      (*list)->l_end = element;
    }
  (*list)->size++;
}

void		my_put_in_list_start(t_list **list, char *str, int id)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->len = my_strlen(str);
  if ((element->e_data = malloc(sizeof(char) * (element->len + 1))) == NULL)
    exit(0);
  my_strcpy(element->e_data, str);
  element->id = id;
  element->e_prev = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_next = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_start->e_prev = element;
      element->e_next = (*list)->l_start;
      (*list)->l_start = element;
    }
  (*list)->size++;
}

t_list		*new_list()
{
  t_list	*list;

  if ((list = malloc(sizeof(t_list))) == NULL)
    exit(0);
  list->size = 0;
  list->l_start = NULL;
  list->l_end = NULL;
  return (list);
}

void		remove_list(t_list **list)
{
  t_element	*tmp;
  t_element	*element;

  if (*list != NULL)
    {
      tmp = (*list)->l_start;
      	while (tmp != NULL)
      	  {
      	    element = tmp;
	    free(tmp->e_data);
      	    tmp = tmp->e_next;
      	    free(element);
      	  }
      free(*list);
      *list = NULL;
    }
}
t_list		*init_list(int ac, char **av)
{
  t_list	*list;
  int		i;

  list = new_list();
  i = 1;
  while (i < ac)
    {
      my_put_in_list_end(&list, av[i], i);
      i++;
    }
  return (list);
}
