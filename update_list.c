/*
** select_word.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec 30 10:36:53 2014 lionel karmes
** Last update Fri Jan  9 18:45:09 2015 lionel karmes
*/

#include "my.h"

void		clear_home()
{
  char		*cap;

  cap = tgetstr("cl", NULL);
  tputs(cap, 1, my_putchar2);
}

void		remove_element_list_current(t_list **list, t_element *element)
{
  my_elementcpy(element, (*list)->l_current);
  remove_element(list, (*list)->l_current);
  if (element->e_prev != NULL)
    (*list)->l_current = element->e_prev;
  else
    (*list)->l_current = (*list)->l_start;
}

void		remove_element_list_select(t_list **list_select,
					   t_element *element)
{
  if ((element = find_element(*list_select, element->id)) != NULL)
      remove_element(list_select, element);
  else
    free(element);
}

int		update_list(t_position *pos, t_list **list_select)
{
  t_element	*element;
  char		*cap;
  
  element = malloc(sizeof(t_element));
  remove_element_list_current(&(pos->list), element);
  remove_element_list_select(list_select, element);
  if (pos->list->l_current == NULL)
    return (END);
  if (pos->cursor.y > 0)
    pos->cursor.y--;
  print_list(pos, *list_select);
  cap = tgetstr("cm", NULL);
  cap = tgoto(cap, 0, pos->cursor.y);
  tputs(cap, 1, my_putchar2);
  return (1);
}
