/*
** move.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 14:24:55 2014 lionel karmes
** Last update Fri Jan  9 18:47:48 2015 lionel karmes
*/

#include "my.h"

extern sig_atomic_t	g_signal_win;

void			get_sigwinch(int sign)
{
  g_signal_win = sign;
}

int			cursor_move(char *c, t_position *pos,
				    t_list **list_select)
{
  int			move_cursor;
  
  move_cursor = 0;
  c[0] = c[1];
  c[1] = c[2];
  c[2] = '\0';
  reverse_mod(*list_select, pos->list->l_current);
  my_putstr0(pos->list->l_current->e_data);
  pos_cursor_begin_word(&(pos->cursor));
  move_cursor = ((my_strcmp(c, K_UP) == 0) ?
		 my_cursor_up(pos) : move_cursor);
  move_cursor = ((my_strcmp(c, K_DOWN) == 0) ?
		 my_cursor_down(pos) : move_cursor);
  move_cursor = ((my_strcmp(c, K_LEFT) == 0) ?
		 my_cursor_left(pos) : move_cursor);
  move_cursor = ((my_strcmp(c, K_RIGHT) == 0) ?
		 my_cursor_right(pos) : move_cursor);
  return (move_cursor);
}

int			read_key_next(t_position *pos, char *c,
				      t_list *list_select)
{
  if (read(0, c, 3) >= 0)
    {
      if (c[1] != 0)
	{
	  cursor_move(c, pos, &list_select);
	  c[0] = 0;
	  c[1] = 0;
	  c[2] = 0;
	}
      else if (c[0] == ' ')
      	select_word(pos, &list_select);
      else if (c[0] == 127)
	if (update_list(pos, &list_select) == END)
	  return (END);
      underline_str(pos, list_select);
    }
  else
    signal_win(pos, list_select);
  return (1);
}

int			read_key(t_position *pos, t_list *list_select)
{
  char			*c;

  if ((c = malloc(sizeof(char) * (3 + 1))) == NULL)
      exit(1);
  c[3] = '\0';
  c[0] = 0;
  c[1] = 0;
  c[2] = 0;
  while (!(c[0] == '\n' || c[0] == K_ECHAP))
    {
      signal(SIGWINCH, get_sigwinch);
      signal_win(pos, list_select);
      if (read_key_next(pos, c, list_select) == END)
	return (END);
    }
  if (c[0] == K_ECHAP)
    return (END);
  free(c);
  return (1);
}
