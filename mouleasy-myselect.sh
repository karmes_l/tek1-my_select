#! /bin/sh

code_down="$(env TERM=xterm-noapp infocmp -1 | grep "cud1" | sed 's/.*=\(.*\),.*/\1/g')"
code_space=" "
code_enter="$(env TERM=xterm-noapp infocmp -1 | grep "cr" | sed 's/.*=\(.*\),.*/\1/g')"

result="$(echo -ne "${code_down}${code_space}${code_enter}" | ./my_select toto OK plop);"
if [ "$result" = "OK" ]
then
  echo "Easy Test: OK"
else
  echo "Easy Test: KO"
fi