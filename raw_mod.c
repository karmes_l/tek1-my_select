/*
** raw_mod.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 16:05:30 2014 lionel karmes
** Last update Fri Jan  9 18:33:17 2015 lionel karmes
*/

#include "my.h"

struct termios		init_termios()
{
  struct termios	t;

  if (tcgetattr(0, &t) == -1)
    {
      my_putstrerror("Error tcgetattr\n");
      exit(1);
    }
  return (t);
}

void			modify_terminal(struct termios *t)
{
  if(tcsetattr(0, TCSANOW, t) == -1)
    {
      my_putstrerror("Error tcsetattr\n");
      exit(1);
    }
}

void			cano_mod(struct termios *t)
{
  t->c_lflag |= ICANON;
  t->c_lflag |= ECHO;
  t->c_cc[VMIN] = 1;
  t->c_cc[VTIME] = 0;
  modify_terminal(t);
}

void			raw_mod(struct termios *t)
{
  t->c_lflag &= ~ICANON;
  t->c_lflag &= ~ECHO;
  modify_terminal(t);
}
