/*
** print_list._select.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select/v5
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Jan  9 18:11:53 2015 lionel karmes
** Last update Fri Jan  9 18:19:51 2015 lionel karmes
*/

#include "my.h"

void	print_list_select(t_list *list)
{
  t_element	*tmp;

  tmp = list->l_start;
  while (tmp != NULL)
    {
      my_putstr(tmp->e_data);
      tmp = tmp->e_next;
      if (tmp != NULL)
	my_putchar(' ');
    }
}
