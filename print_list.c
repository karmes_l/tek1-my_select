/*
** print_list.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select/v3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan  5 12:05:29 2015 lionel karmes
** Last update Fri Jan  9 18:27:46 2015 lionel karmes
*/

#include "my.h"

extern sig_atomic_t	g_signal_win;

void			print_element(t_element *tmp, t_list *list,
				      t_list *list_select)
{
  char			*cap;

  if (tmp->id == list->l_current->id)
    underline(list, list_select);
  else
    reverse_mod(list_select, tmp);
  my_putstr0(tmp->e_data);
  cap = tgetstr("ue", NULL);
  tputs(cap, 1, my_putchar2);
}

void			raz_static_print_list_valid(int *x, int *y,
						    t_cursor *cursor)
{
  *x = 0;
  *y = 0;
  free(cursor->len_column);
  if ((cursor->len_column = malloc(sizeof(int))) == NULL)
    exit(1);
  cursor->size = 0;
}


void		print_list_valid_next(t_element *tmp, t_position *pos,
				      int x, int y)
{
  if (tmp->id == pos->list->l_current->id)
    {
      pos->cursor.x = x;
      pos->cursor.y = y;
    }
  if (y == 0)
    {
      pos->cursor.size++;
      if (pos->cursor.size > 1)
	pos->cursor.len_column =
	  my_int_tab_realloc(pos->cursor.len_column,
			     pos->cursor.size - 1, pos->cursor.size);
      pos->cursor.len_column[pos->cursor.size - 1] = 0;
    }
}

int			print_list_valid(t_element *tmp, struct winsize *win,
					 t_list *list_select, t_position *pos)
{
  static int		x = 0;
  static int	       	y = 0;

  if (tmp->id == pos->list->l_start->id)
    raz_static_print_list_valid(&x, &y, &(pos->cursor)); 
  print_list_valid_next(tmp, pos, x, y);
  if (pos_cursor_x(x, &pos->cursor) + tmp->len > win->ws_col)
    {
      clear_home();
      my_putstrerror("Veuillez redimenssioner la taille de la fenêtre");
      return (0);
    }
  print_element(tmp, pos->list, list_select);
  pos->cursor.len_column[pos->cursor.size - 1] =
    MAX(pos->cursor.len_column[pos->cursor.size - 1], tmp->len);
  if (y >= win->ws_row - 1)
    {
      x++;
      y = 0;
    }
  else
    y++;
  pos_cursor_choice(pos_cursor_x(x, &pos->cursor), y);
  return (1);
}
 

int			print_list(t_position *pos, t_list *list_select)
{
  int			i;
  struct winsize	win;
  t_element		*tmp;

  i = 0;
  ioctl(0, TIOCGWINSZ, &win);
  tmp = pos->list->l_start;
  clear_home();
  while (i < pos->list->size)
    {
      if(!print_list_valid(tmp, &win, list_select, pos))
	return (0);
      i++;
      tmp = tmp->e_next;
    }
  pos_cursor_choice(pos_cursor_x(pos->cursor.x, &pos->cursor), pos->cursor.y);
  return (1);
}
