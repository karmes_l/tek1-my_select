/*
** my_putchar2.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select/v4
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Jan  9 17:35:33 2015 lionel karmes
** Last update Fri Jan  9 18:34:51 2015 lionel karmes
*/

#include "my.h"

int	my_putchar2(int c)
{
  write(0, (char *) &c, 1);
  return (0);
}

void	my_putchar0(char c)
{
  write(0, &c, 1);
}

void	my_putstr0(char *c)
{
  int	i;

  i = 0;
  while (c[i] != '\0')
    {
      my_putchar0(c[i]);
      i++;
    }
}
