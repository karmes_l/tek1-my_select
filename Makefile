##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Fri Jan  9 18:14:03 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= my_select

LIB	= libmy

SRCS	= main.c \
	my_select.c \
	init_list.c \
	init_list_next.c \
	print_list.c \
	cursor_move.c \
	fct_cursor_move.c \
	fct_cursor_move2.c \
	raw_mod.c \
	position_cursor.c \
	select_word.c \
	update_list.c \
	my_int_tab_realloc.c \
	my_putchar2.c \
	print_list_select.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib/
	$(CC) $(OBJS) -o $(NAME) $(CFLAGS) $(LDFLAGS) -L./lib/ -lmy -lncurses

clean:
	$(RM) $(OBJS)
	make clean -C lib/

fclean: clean
	make fclean -C lib/
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
