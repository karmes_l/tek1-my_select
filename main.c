/*
** main.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 10:41:25 2014 lionel karmes
** Last update Fri Jan  9 18:31:18 2015 lionel karmes
*/

#include "my.h"

int	main(int argc, char **argv)
{
  if (argc > 1)
    my_select(argc, argv);
  else
    my_putstrerror("Veuillez au moins rentrer un choix\n");
  return (0);
}
