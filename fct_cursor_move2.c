/*
** move.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 14:22:16 2014 lionel karmes
** Last update Fri Jan  9 17:37:23 2015 lionel karmes
*/

#include "my.h"

int	my_cursor_left2(t_position *pos)
{
  struct winsize	win;
  int			i;

  if (pos->cursor.x > 0)
    {
      pos->cursor.x--;
      i = 0;
      ioctl(0, TIOCGWINSZ, &win);
      while (i < win.ws_row)
	{
	  pos->list->l_current = pos->list->l_current->e_prev;
	  i++;
	}
      return (1);
    }
  return (0);
}

int	my_cursor_left(t_position *pos)
{
  if (my_cursor_left2(pos))
    return (1);
  else
    {
      while (pos->cursor.x < pos->cursor.size - 1)
	my_cursor_right(pos);
    }
  pos_cursor_begin_word(&(pos->cursor));
  return (1);
}

int	my_cursor_right2(t_position *pos)
{
  struct winsize	win;
  int			i;

  if (pos->cursor.x < pos->cursor.size - 1)
    {
      pos->cursor.x++;
      i = 0;
      ioctl(0, TIOCGWINSZ, &win); 
      while (i < win.ws_row && pos->list->l_current->e_next != NULL)
	{
	  pos->list->l_current = pos->list->l_current->e_next;
	  i++;
	}
      pos->cursor.y -= win.ws_row - i;
      return (1);
    }
  return (0);
}

int	my_cursor_right(t_position *pos)
{
  pos = pos;
  if (my_cursor_right2(pos))
    return (1);
  else
    {
      while (pos->cursor.x > 0)
	my_cursor_left(pos);
    }
  pos_cursor_begin_word(&(pos->cursor));
  return (1);
}
