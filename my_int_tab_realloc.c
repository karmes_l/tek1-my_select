/*
** my_int_tab_realloc.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select/v3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Jan  6 13:54:47 2015 lionel karmes
** Last update Tue Jan  6 19:11:14 2015 lionel karmes
*/

#include "my.h"

int	*my_int_tab_realloc(int *tabb, int size_current, int size_after)
{
  int	*new_tab;
  int	i;
  
  new_tab = NULL;
  if ((new_tab = malloc(sizeof(int) * size_after)) == NULL)
    exit(1);
  if (tabb != NULL)
    {
      i = 0;
      while (i < size_current)
      	{
      	  new_tab[i] = tabb[i];
      	  i++;
      	}
      free(tabb);
    }
  return (new_tab);
}
