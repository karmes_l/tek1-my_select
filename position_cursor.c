/*
** position_cursor.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 29 09:45:37 2014 lionel karmes
** Last update Fri Jan  9 17:29:40 2015 lionel karmes
*/

#include "my.h"

int	pos_cursor_x(int x_cursor, t_cursor *cursor)
{
  int	i;
  int	x;

  i = 0;
  x = 0;
  while (i < x_cursor)
    {
      x += cursor->len_column[i] + 1;
      i++;
    }
  return (x);
}

void	pos_cursor_begin_word(t_cursor *cursor)
{
  char	*cap;

  cap = tgetstr("cm", NULL);
  cap = tgoto(cap, pos_cursor_x(cursor->x, cursor), cursor->y);
  tputs(cap, 1, my_putchar2);
}

void	pos_cursor_choice(int x, int y)
{
  char			*cap;

  cap = tgetstr("cm", NULL);
  cap = tgoto(cap, x, y);
  tputs(cap, 1, my_putchar2);
}

void	pos_cursor_end()
{
  char	*cap;

  cap = tgetstr("se", NULL);
  tputs(cap, 1, my_putchar2);
  cap = tgetstr("ve", NULL);
  tputs(cap, 1, my_putchar2);
  cap = tgetstr("cl", NULL);
  tputs(cap, 1, my_putchar2);
}
