/*
** select_word.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec 30 10:36:53 2014 lionel karmes
** Last update Fri Jan  9 18:26:47 2015 lionel karmes
*/

#include "my.h"

void		reverse_mod(t_list *list_select, t_element *element)
{
  char		*cap;

  if (find_element(list_select, element->id) != NULL)
    cap = tgetstr("mr", NULL);
  else
    cap = tgetstr("se", NULL);
  tputs(cap, 1 ,my_putchar2);
}

void		underline(t_list *list, t_list *list_select)
{
  char		*cap;

  reverse_mod(list_select, list->l_current);
  cap = tgetstr("us", NULL);
  tputs(cap, 1, my_putchar2);
}

void		underline_str(t_position *pos, t_list *list_select)
{
  char		*cap;

  underline(pos->list, list_select);
  pos_cursor_begin_word(&(pos->cursor));
  my_putstr0(pos->list->l_current->e_data);
  pos_cursor_begin_word(&(pos->cursor));
  cap = tgetstr("ue", NULL);
  tputs(cap, 1, my_putchar2);
}

void		select_word(t_position *pos, t_list **list_select)
{
  t_element	*element;

  if ((element = find_element(*list_select, pos->list->l_current->id)) != NULL)
    remove_element(list_select, element);
  else
    {
      my_put_in_list_end(list_select, pos->list->l_current->e_data,
			 pos->list->l_current->id);
      reverse_mod(*list_select, pos->list->l_current);
      pos_cursor_begin_word(&(pos->cursor));
      my_putstr0(pos->list->l_current->e_data);
      my_cursor_down(pos);
    }
}
