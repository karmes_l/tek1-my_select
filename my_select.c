/*
** my_select.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 11:18:25 2014 lionel karmes
** Last update Sat Jan 10 14:08:47 2015 lionel karmes
*/

#include "my.h"

sig_atomic_t		g_signal_win = SIGWINCH;

void			init_start(t_position *pos, int ac ,char **av)
{
  char			*cap;
  t_list		*list;
  t_cursor		cursor;

  list = init_list(ac, av);
  if (tgetent(NULL, "xterm") != 1)
    exit(0);
  list->l_current = list->l_start;
  pos->list = list;
  cursor.y = 0;
  cursor.x = 0;
  pos->cursor = cursor;
  if ((pos->cursor.len_column = malloc(sizeof(int))) == NULL)
    exit(1);
  cap = tgetstr("vi", NULL);
  tputs(cap, 1, my_putchar2);
}

void			signal_win(t_position *pos, t_list *list_select)
{
  if (g_signal_win == SIGWINCH)
    {
      while (!print_list(pos, list_select))
	{
	  while (g_signal_win != SIGWINCH)
	    signal(SIGWINCH, get_sigwinch);
	  g_signal_win = 0;
	}
      g_signal_win = 0;
    }
}

int			my_select(int ac, char **av)
{
  t_position		pos;
  t_list		*list_select;
  struct termios	t;

  list_select = new_list();
  t = init_termios();
  raw_mod(&t);
  init_start(&pos, ac, av);
  signal_win(&pos, list_select);
  pos_cursor_choice(0, 0);
  if (read_key(&pos, list_select) != END)
    print_list_select(list_select);
  pos_cursor_end();
  remove_list(&(pos.list));
  free(pos.cursor.len_column);
  remove_list(&list_select);
  cano_mod(&t);
  return (0);
}
