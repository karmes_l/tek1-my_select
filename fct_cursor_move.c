/*
** move.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 14:22:16 2014 lionel karmes
** Last update Fri Jan  9 17:42:08 2015 lionel karmes
*/

#include "my.h"

int			my_cursor_up2(t_position *pos)
{
  struct winsize	win;

  if (pos->cursor.y == 0)
    {
      ioctl(0, TIOCGWINSZ, &win);
      while (pos->list->l_current->e_next != NULL
	     && pos->cursor.y < win.ws_row - 1)
	my_cursor_down(pos);
      return (1);
    }
  return (0);
}

int	my_cursor_up(t_position *pos)
{
  char	*cap;

  if (my_cursor_up2(pos))
    return (1);
  else
    {
      pos->list->l_current = pos->list->l_current->e_prev;
      cap = tgetstr("up", NULL);
      pos->cursor.y--;
    }
  tputs(cap, 1, my_putchar2);
  return (1);
}

int	my_cursor_down2(t_position *pos)
{
  struct winsize	win;

  ioctl(0, TIOCGWINSZ, &win);
  if (pos->cursor.y == win.ws_row - 1 || pos->list->l_current->e_next == NULL)
    {
      while (pos->cursor.y > 0)
	my_cursor_up(pos);
      return (1);
    }
  return (0);
}

int	my_cursor_down(t_position *pos)
{
  char	*cap;

  if (my_cursor_down2(pos))
    return (1);
  else
    {
      pos->list->l_current = pos->list->l_current->e_next;
      cap = tgetstr("do", NULL);
      pos->cursor.y++;
    }
  tputs(cap, 1, my_putchar2);
  return (1);
}
